variable "aws_region" {
  default     = "ap-southeast-1"
  type        = string
  description = "Region in which AWS Resources to be created"
}

# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type        = string
  default     = "dev"
}

# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type        = string
  default     = "HR"
}
