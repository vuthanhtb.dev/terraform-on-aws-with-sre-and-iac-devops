variable "aws_region" {
  default     = "ap-southeast-1"
  type        = string
  description = "Region in which AWS Resources to be created"
}
