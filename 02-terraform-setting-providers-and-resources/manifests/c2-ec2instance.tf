resource "aws_instance" "myec2vm" {
  ami = "ami-0b94777c7d8bfe7e3"
  instance_type = "t2.micro"
  user_data = file("${path.module}/app1-install.sh")
  tags = {
    "Name" = "EC2 Demo"
  }
}
