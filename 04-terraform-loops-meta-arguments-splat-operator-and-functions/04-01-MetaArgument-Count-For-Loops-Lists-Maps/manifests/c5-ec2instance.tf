resource "aws_instance" "ec2vm" {
  ami                    = data.aws_ami.amzlinux2.id
  instance_type          = var.instance_type_list[1]
  key_name               = var.instance_keypair
  user_data              = file("${path.module}/app1-install.sh")
  count                  = 2
  vpc_security_group_ids = [
    aws_security_group.vpc_ssh.id,
    aws_security_group.vpc_web.id
  ]
  tags                   = {
    Name = "Count-Demo-${count.index}"
  }
}
