variable "aws_region" {
  default     = "ap-southeast-1"
  type        = string
  description = "Region in which AWS Resources to be created"
}

variable "instance_keypair" {
  default     = "terraform-key"
  type        = string
  description = "AWS EC2 Key pair that need to be associated with EC2 Instance"
}

# AWS EC2 Instance Type - List
variable "instance_type_list" {
  description = "EC2 Instnace Type"
  type        = list(string)
  default     = ["t2.micro", "t2.small"]
}


# AWS EC2 Instance Type - Map
variable "instance_type_map" {
  description = "EC2 Instnace Type"
  type        = map(string)
  default     = {
    "dev"  = "t2.micro"
    "qa"   = "t2.small"
    "prod" = "t2.large"
  }
}