# Availability Zones Datasource
data "aws_availability_zones" "my_azones" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

resource "aws_instance" "ec2vm" {
  ami                    = data.aws_ami.amzlinux2.id
  instance_type          = var.instance_type_map["dev"]
  key_name               = var.instance_keypair
  user_data              = file("${path.module}/app1-install.sh")
  # Create EC2 Instance in all Availabilty Zones of a VPC  
  for_each = toset(data.aws_availability_zones.my_azones.names)
  availability_zone = each.key # You can also use each.value because for list items each.key == each.value
  vpc_security_group_ids = [
    aws_security_group.vpc_ssh.id,
    aws_security_group.vpc_web.id
  ]
  tags                   = {
    Name = "For-Each-Demo-${each.key}"
  }
}
