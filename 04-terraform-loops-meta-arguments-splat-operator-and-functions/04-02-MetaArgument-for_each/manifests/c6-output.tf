# EC2 Instance Public IP with TOSET
output "instance_publicip" {
  description = "EC2 Instance Public IP"
  #value = aws_instance.ec2vm.*.public_ip   # Legacy Splat
  #value = aws_instance.ec2vm[*].public_ip  # Latest Splat
  value = toset([
      for ec2vm in aws_instance.ec2vm : ec2vm.public_ip
    ])  
}

# EC2 Instance Public DNS with TOSET
output "instance_publicdns" {
  description = "EC2 Instance Public DNS"
  #value = aws_instance.ec2vm[*].public_dns  # Legacy Splat
  #value = aws_instance.ec2vm[*].public_dns  # Latest Splat
  value = toset([
      for ec2vm in aws_instance.ec2vm : ec2vm.public_dns
    ])    
}

# EC2 Instance Public DNS with MAPS
output "instance_publicdns2" {
  value = tomap({
    for s, ec2vm in aws_instance.ec2vm : s => ec2vm.public_dns
    # S intends to be a subnet ID
  })
}
