variable "aws_region" {
  default     = "ap-southeast-1"
  type        = string
  description = "Region in which AWS Resources to be created"
}

variable "instance_keypair" {
  default     = "terraform-key"
  type        = string
  description = "AWS EC2 Key pair that need to be associated with EC2 Instance"
}
