output "instance_public_ip" {
  value = aws_instance.ec2vm.public_ip
  description = "EC2 Instance Public IP"
}

output "instance_public_dns" {
  value = aws_instance.ec2vm.public_dns
  description = "EC2 Instance Public DNS"
}
